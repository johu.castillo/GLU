<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//importar modelos
use App\User;
use App\Modelos\Autor;
//para correo
use Mail;
use App\Mail\Notificaciones;

class UsuariosController extends Controller
{
    public function obtener_usuarios(){
        return response()->json(User::where('status','!=','9')->get());
    }

    public function contar_usuarios(){
        return response()->json(User::all()->count());
    }

    public function obtener_usuario($id){
        $usuario = User::find($id);
        if(!empty($usuario)){
        return response()->json($usuario);}
        else{
            return response()->json('que pena :(');
        }
    }

    public function borrar_usuario($id){
        $usuario = User::find($id);
        if(!empty($usuario)){
            $usuario->status = 9;
            $usuario->save();
            return response()->json('listo');}
        else{
            return response()->json('no hay nada que borrar');
        }
    }

    //Metodo 1
    public function crear_usuario(Request $request){
        $usuario = User::create($request->all());
         //Mail::to('aztrocastillo28@gmail.com')->send(new Notificaciones('Se registro nuevo usuario.'));

        return response()->json('listo');
    }

    public function editar_usuario(Request $request,$id){
        $usuario = User::find($id);
        $usuario->name =  $request->name;
        $usuario->primer_nombre =  $request->primer_nombre;
        $usuario->apellido =  $request->apellido;
        $usuario->email =  $request->email;
        $usuario->password =  $request->password;
        $usuario->save();
        return response()->json('listo');
    }

    

    //Metodo 2
    /*public function crear_usuario(Request $request){
        $usuario = User::create([
            'name' =>  $request->name,
            'primer_nombre' =>  $request->primer_nombre,
            'apellido' =>  $request->apellido,
            'email' =>  $request->email,
            'password' =>  $request->password,
        ]);
        return response()->json('listo');
    }*/


    /*
    public function obtener_usuario($id){
        return response()->json(User::where('id',$id)->first());
    }*/



}
