<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Libros extends Model
{
    protected $table = 'libros';
    
    protected $fillable = [
        'nombre','descripcion','status'
    ];
}
