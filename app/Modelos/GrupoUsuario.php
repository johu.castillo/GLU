<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class GrupoUsuario extends Model
{
    protected $table = 'user_group';
    
    protected $fillable = [
        'contexto','rol_id','user_id','user_group_id','status'
    ];
}
