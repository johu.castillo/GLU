<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    protected $table = 'publication';
    
    protected $fillable = [
        'date','volumen','capitulo','titulo','year','autor_id','libros_id','status'
    ];
}
