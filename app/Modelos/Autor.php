<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    //tabla a la que esta ligada el modelo
    protected $table = 'autor';
//datos llenables
    protected $fillable = [
        'primer_nombre','apellido','email','user_group_id','status'
    ];

}
