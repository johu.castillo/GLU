<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace("Api")->group(function(){
    
    Route::get('ObtenerUsuarios', [
    'uses'=> 'UsuariosController@obtener_usuarios',
    'as'=> 'usuarios.obtener',
    ]);

    Route::get('ContarUsuarios', [
        'uses'=> 'UsuariosController@contar_usuarios',
        'as'=> 'usuarios.contar',
    ]);
    

    Route::get('ObtenerUsuario/{id}', [
        'uses'=> 'UsuariosController@obtener_usuario',
        'as'=> 'usuario.obtener',
        ]);
    
    Route::get('BorrarUsuario/{id}', [
        'uses'=> 'UsuariosController@borrar_usuario',
        'as'=> 'usuario.borrar',
        ]);
    
    Route::post('CrearUsuario/', [
        'uses'=> 'UsuariosController@crear_usuario',
        'as'=> 'usuario.crear',
        ]);

    Route::post('EditarUsuario/{id}', [
        'uses'=> 'UsuariosController@editar_usuario',
        'as'=> 'usuario.editar',
        ]);

});
