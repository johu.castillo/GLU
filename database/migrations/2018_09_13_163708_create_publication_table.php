<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publication', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('volumen');
            $table->integer('capitulo');
            $table->string('titulo');
            $table->string('year');
            $table->unsignedInteger('autor_id');
            $table->foreign('autor_id')->references('id')->on('autor');
            $table->unsignedInteger('libros_id');
            $table->foreign('libros_id')->references('id')->on('libros');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publication');
    }
}
